import { assert as asset } from 'chai';
import * as app from '../app.js'

describe('App', () => {
    //тот же результат
    it('Должно говорить hello', () => {
        asset.equal(app.SayHello(), 'hello');
    })
    //Тот же тип
    it('Должно возвращать string', () => {
        asset.typeOf(app.SayHello(), 'string');
    })
    //Больше чем
    it('1 + 2 = 3', () => {
        asset.isAbove(app.TwoNumbersPlus(1, 2), 1);
    })
    //Верное выражение
    it('5 + 7 = 12', () => {
        asset.isTrue(app.TwoNumbersPlus(5, 7) == 12);
    })
})